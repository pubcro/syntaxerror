## How Can I Contribute?

### Reporting Bugs

#### How Do I Submit A (Good) Bug Report?
Bugs are tracked as [GitLab issues](https://www.gitlab.com/pubcro/syntaxerror/issues). Create an issue on that repository and provide the following information by filling in [the template](ISSUE_TEMPLATE.md).

Explain the problem and include additional details to help maintainers reproduce the problem:
* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible.
* **When listing steps, don't just say what you did, but explain how you did it.**
* **Provide specific examples to demonstrate the steps**. Include links or Urls
* **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
* **Explain which behavior you expected to see instead and why.**
* **If the problem is related to performance or memory**, include a profile of the server used to run syntaxerror
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:
* **Can you reproduce the problem ?**
* **Did the problem start happening recently** (e.g. after updating to a new version) or **was this always a problem** ?
* **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.


### Suggesting Enhancements or request new feature

#### How Do I Submit A (Good) Enhancement Suggestion?
* **Use a clear and descriptive title** for the issue to identify the suggestion.
* **Provide a step-by-step description of the suggested enhancement** in as many details as possible.
* **Explain why this enhancement would be useful to most syntaxerror users.**
* **Focusing on what is best for the community before to file a new enhancement issue request**. syntaxerror is opensource and we want to provide new features can be used to the most syntaxerror users.

### Wha't you should not post as an issue ?
* **Duplicate issue**. Try to search on the open issue to avoid any duplicate issue.
* **Already closed issue**. Before to post an issue, ensure you run the latest version of syntaxerror.
* **Please don't file an issue to ask a question**. Contact a syntaxerror core dev team member directly by mail or chat.

#### Type of Issue and Issue State

| Label name | Description |
| --- | --- |
| `bug` | Bugs request. |
| `blocked` | Issues blocked by other(s) issue(s). |
| `confirmed-bug` | Confirmed bugs request. |
| `confirmed-enhancement` | Confirmed feature request. |
| `duplicate` | Issues which are duplicates of other issues, i.e. they have been reported before. |
| `enhancement` | Feature requests. |
| `invalid` | Issues which aren't valid (e.g. user errors). |
| `wontfix` | The syntaxerror core team has decided not to fix these issues for now, either because they're working as intended or for some other reason. |
