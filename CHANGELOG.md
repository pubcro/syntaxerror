# Change Log
All notable changes to this project will be documented in this file.

## 1.2.1 [latest-stable] - 2022-03-09
### Added 
- Add support for debian bullseye
- Add multi threading support

## 1.1.5 [latest-stable] - 2019-12-30
### Fixed
- fix installation process on npm modules

## 1.1.4 - 2019-10-11
### Added
- Add support for Debian Buster distribution
- Add support for Golang .go file
- Add support for Toml .toml file
- Update nodeJS to v12.x

### Fix
- Fix somes minor bug during installation
- Fix dependencies update during install / upgrade process

## 1.1.3
### Fix
- even if -f option is used, syntaxerror will exit with error code 1 at the end of the process if one or more error are found

### Security
- You can pass any `find` argument after -f or --force exept `-exec` or `-delete`

## 1.1.2 
### Added 
- acorn dependencies during installation process

### Fix
- setup issue with a restrictive root umask

## 1.1.1 - 2017-12-12
### Added
- support for CSS

## 1.1 - 2017-12-11
### Added
- first packaged version
- add support for
  - bash 
  - javascript
  - json
  - perl
  - php
  - ruby
  - python
  - xml
  - yaml

- add option -f|--force to force syntaxerror to continue to parse other file after detectig an error
