#!/bin/bash
# Self 
readonly libcommon_rev="5.3.22030017"
readonly libcommon=true
readonly rootdir=$(pwd)
readonly self=$(basename $0)
readonly selfdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Terminal Color definition
readonly _cred="[0;31m"  # RED
readonly _cgrn="[0;32m"  # GREEN
readonly _cyel="[0;33m"  # YELLOW
readonly _cblu="[0;34m"  # BLUE
readonly _cpur="[0;35m"  # PURPLE
readonly _ccya="[0;36m"  # CYAN
readonly _cwhi="[0;37m"  # WHITE
readonly _cbred="[1;31m" # H-LIGHT RED
readonly _cbgre="[1;32m" # H-LIGHT GREEN
readonly _cbyel="[1;33m" # H-LIGHT YELLOW
readonly _cbblu="[1;34m" # H-LIGHT BLUE
readonly _cbcya="[1;36m" # CYAN
readonly _cbpur="[1;35m" # H-LIGHT PURPLE
readonly _cbwhi="[1;37m" # WHITE
readonly _crst="[0m"     # Reset color

readonly ok_panel="$_cbblu[$_cbgre*$_cbblu]$_crst"
readonly warn_panel="$_cbblu[$_cbyel*$_cbblu]$_crst"
readonly err_panel="$_cbblu[$_cbred*$_cbblu]$_crst"
readonly ques_panel="$_cbblu[$_cbcya?$_cbblu]$_crst"
readonly warn_signal="$_cred/$_cbyel!$_cred\\$_crst"

readonly Ny_panel="$_cbblu[${_cbwhi}N$_cbblu|${_crst}y$_cbblu]$_crst"
readonly Yn_panel="$_cbblu[${_cbwhi}Y$_cbblu|${crst}n$_cbblu]$_crst"

readonly ok_flag="$_cbwhi[ ${_cbgre} ok ${_cbwhi} ]$_crst"
readonly done_flag="$_cbwhi[ ${_cbgre}done${_cbwhi} ]$_crst"
readonly warn_flag="$_cbwhi[ ${_cbyel}warn${_cbwhi} ]$_crst"
readonly err_flag="$_cbwhi[ ${_cbred}error${_cbwhi} ]$_crst"

# lANG FLAG
lang_flag() {
  local lANG=$1
  echo -n "$_cbwhi[$_cbpur $lANG ${_cbwhi}]$_crst"
}

# Env/Core/System definition
# error code mappings from errno.h
readonly EINVAL=22   # Invalid argument
readonly EMFILE=24   # Too many open files
readonly RET_SUCCESS='return 0'
readonly SUCCESS=0
readonly RET_ERROR='return 1'
readonly ERROR=1
readonly PROCID=$$
readonly __self=$(echo ${self} | tr "." "_")

# Env
__p_cleanall() {
        $RET_SUCCESS
} ; readonly -f __p_cleanall

set_return() {
  local _return_value=$1
  RETURN=${RETURN:=0}
  if [[ $_return_value -gt $RETURN ]] ; then
    RETURN=$_return_value
  fi
  $RET_SUCCESS
} ; set_return

function maydie () {
  if [[ $paranoid == "true" ]] ; then
    __p_cleanall
    exit $ERROR
  else
    $RET_ERROR
  fi
} ; readonly -f maydie

function cmd() {
  $@
}

function cmd1null() {
  $@ >/dev/null
}

function cmd2null() {
  $@ 2>/dev/null
}

function cmdnull () {
  $@ &>/dev/null
}

# end-of-jobs marker
job_pool_end_of_jobs="JOBPOOL_END_OF_JOBS"

# job queue used to send jobs to the workers
job_pool_job_queue=/tmp/job_pool_job_queue_$$

# where to run results to
job_pool_result_log=/tmp/job_pool_result_log_$$

# toggle command echoing
job_pool_echo_command=0

# number of parallel jobs allowed.  also used to determine if job_pool_init
# has been called when jobs are queued.
job_pool_pool_size=-1

# \brief variable to check for number of non-zero exits
job_pool_nerrors=0

################################################################################
# private functions
################################################################################

# \brief debug output
function _job_pool_echo()
{
    if [[ "${job_pool_echo_command}" == "1" ]]; then
        echo $@
    fi
}

# \brief cleans up
function _job_pool_cleanup()
{
    rm -f ${job_pool_job_queue} ${job_pool_result_log}
}

# \brief signal handler
function _job_pool_exit_handler()
{
    _job_pool_stop_workers
    _job_pool_cleanup
}

# \brief print the exit codes for each command
# \param[in] result_log  the file where the exit codes are written to
function _job_pool_print_result_log()
{
    job_pool_nerrors=$(grep ^ERROR "${job_pool_result_log}" | wc -l)
    #cat "${job_pool_result_log}" | sed -e 's/^ERROR//'
}

# \brief the worker function that is called when we fork off worker processes
# \param[in] id  the worker ID
# \param[in] job_queue  the fifo to read jobs from
# \param[in] result_log  the temporary log file to write exit codes to
function _job_pool_worker()
{
    local id=$1
    local job_queue=$2
    local result_log=$3
    local cmd=
    local args=

    exec 7<> ${job_queue}
    while [[ "${cmd}" != "${job_pool_end_of_jobs}" && -e "${job_queue}" ]]; do
        # workers block on the exclusive lock to read the job queue
        flock --exclusive 7
        IFS=$'\v'
        read cmd args <${job_queue}
        set -- ${args}
        unset IFS
        flock --unlock 7
        # the worker should exit if it sees the end-of-job marker or run the
        # job otherwise and save its exit code to the result log.
        if [[ "${cmd}" == "${job_pool_end_of_jobs}" ]]; then
            # write it one more time for the next sibling so that everyone
            # will know we are exiting.
            echo "${cmd}" >&7
        else
            #_job_pool_echo "### _job_pool_worker-${id}: ${cmd}"
            # run the job
            { ${cmd} "$@" ; }
            # now check the exit code and prepend "ERROR" to the result log entry
            # which we will use to count errors and then strip out later.
            local result=$?
            local status=
            if [[ "${result}" != "0" ]]; then
                status=ERROR
            fi
            # now write the error to the log, making sure multiple processes
            # don't trample over each other.
            exec 8<> ${result_log}
            flock --exclusive 8
            _job_pool_echo "${status}job_pool: exited ${result}: ${cmd} $@" >> ${result_log}
            flock --unlock 8
            exec 8>&-
            #_job_pool_echo "### _job_pool_worker-${id}: exited ${result}: ${cmd} $@"
        fi
    done
    exec 7>&-
}

# \brief sends message to worker processes to stop
function _job_pool_stop_workers()
{
    # send message to workers to exit, and wait for them to stop before
    # doing cleanup.
    echo ${job_pool_end_of_jobs} >> ${job_pool_job_queue}
    wait
}

# \brief fork off the workers
# \param[in] job_queue  the fifo used to send jobs to the workers
# \param[in] result_log  the temporary log file to write exit codes to
function _job_pool_start_workers()
{
    local job_queue=$1
    local result_log=$2
    for ((i=0; i<${job_pool_pool_size}; i++)); do
        _job_pool_worker ${i} ${job_queue} ${result_log} &
    done
}

################################################################################
# public functions
################################################################################

# \brief initializes the job pool
# \param[in] pool_size  number of parallel jobs allowed
# \param[in] echo_command  1 to turn on echo, 0 to turn off
function job_pool_init()
{
    local pool_size=$1
    local echo_command=$2

    # set the global attibutes
    job_pool_pool_size=${pool_size:=1}
    job_pool_echo_command=${echo_command:=0}

    # create the fifo job queue and create the exit code log
    rm -rf ${job_pool_job_queue} ${job_pool_result_log}
    mkfifo ${job_pool_job_queue}
    touch ${job_pool_result_log}

    # fork off the workers
    _job_pool_start_workers ${job_pool_job_queue} ${job_pool_result_log}
}

# \brief waits for all queued up jobs to complete and shuts down the job pool
function job_pool_shutdown()
{
    _job_pool_stop_workers
    _job_pool_print_result_log
    _job_pool_cleanup
}

# \brief run a job in the job pool
function job_pool_run()
{
    if [[ "${job_pool_pool_size}" == "-1" ]]; then
        job_pool_init
    fi
    printf "%s\v" "$@" >> ${job_pool_job_queue}
    echo >> ${job_pool_job_queue}
}

# \brief waits for all queued up jobs to complete before starting new jobs
# This function actually fakes a wait by telling the workers to exit
# when done with the jobs and then restarting them.
function job_pool_wait()
{
    _job_pool_stop_workers
    _job_pool_start_workers ${job_pool_job_queue} ${job_pool_result_log}
}

for argv in $@ ; do
  case $argv in
    -f|--force)
      readonly paranoid=false
      shift
    ;;
    -v|--verbose)
      readonly verbose=true
      shift
    ;;
    -t|--textout)
      readonly textout=true
    ;;
  esac
done

echo $@ | grep "-exec" &>/dev/null
RC=$?
if [[ $RC == 0 ]] ; then
  echo "exec option is not allowed"
  exit 1
fi

echo $@ | grep "-delete" &>/dev/null
RC=$?
if [[ $RC == 0 ]] ; then
  echo "delete option is not allowed"
  exit 1
fi

lang_wraper_debug() {
  local file=$2
  if [[ ! -r $file ]] ; then
    maydie "can't read $file"
  fi
  case $1 in
    bash)
      bash -n $file
      $RET_SUCCESS
      ;;
    css)
      csslint --warnings=none --quiet $file
      $RET_SUCCESS
      ;;
    go)
      golint -min_confidence 1.0 $file
      $RET_SUCCESS
      ;;
    javascript)
      acorn $file 
      $RET_SUCCESS
      ;;
    json)
      #jsonlint $file 
      cat $file | jq '.'
      $RET_SUCCESS
      ;;
    perl)
      perl -c $file
      $RET_SUCCESS
      ;;
    php)
      php -l $file
      $RET_SUCCESS
      ;;
    ruby)
      ruby -c $file
      $RET_SUCCESS
      ;;
    toml)
      tomlv $file
      $RET_SUCCESS
      ;;
    xml)
      #python -c "import sys, xml.dom.minidom as d; d.parse(sys.argv[1])" $file 
      $RET_SUCCESS
      ;;
    yaml)
      perl -MYAML -e "use YAML;YAML::LoadFile('$file')"
      $RET_SUCCESS
      ;;
  esac
}

lang_wraper() {
  unset RC
  local file=$2
  if [[ ! -r $file ]] ; then
    maydie "can't read $file"
  fi
  case $1 in
    bash)
      bash -n $file >> /dev/null 2>&1
      local RC=$?
      lANG="BASH"
      ;;
    css)
      csslint --warnings=none --quiet $file >> /dev/null 2>&1
      local RC=$?
      lANG=" CSS"
      ;;
    go)
      golint -min_confidence 1.0 -set_exit_status $file >> /dev/null 2>&1
      local RC=$?
      LANG=" GO "
      ;;
    javascript)
      acorn --silent $file >> /dev/null 2>&1
      local RC=$?
      lANG=" JS "
      ;;
    json)
      #jsonlint -q $file >> /dev/null 2>&1
      cat $file | jq '.' >> /dev/null 2>&1
      local RC=$?
      lANG="JSON"
      ;;
    perl)
      perl -c $file >> /dev/null 2>&1
      local RC=$?
      lANG="PERL"
      ;;
    php)
      php -l $file >> /dev/null 2>&1
      local RC=$?
      lANG=" PHP"
      ;;
    ruby)
      ruby -c $file >> /dev/null 2>&1
      local RC=$?
      lANG="RUBY"
      ;;
    toml)
      tomlv $file >> /dev/null 2>&1
      local RC=$?
      LANG="TOML"
      ;;
    xml)
      python -c "import sys, xml.dom.minidom as d; d.parse(sys.argv[1])" $file >> /dev/null 2>&1
      local RC=$?
      lANG=" XML"
      ;;
    yaml)
      perl -MYAML -e "use YAML;YAML::LoadFile('$file')" >> /dev/null 2>&1
      local RC=$?
      lANG="YAML"
      ;;
  esac
if [[ $RC == 0 ]] ; then
  echo "$ok_panel $ok_flag " $(lang_flag $lANG) " $file"
  lANG="UNKW"
  $RET_SUCCESS
else
  set_return 1
  echo "$err_panel $err_flag" $(lang_flag $lANG) " $file"
  lANG="UNKW"
  $RET_ERROR
fi
}
poolSize=$(cat /proc/cpuinfo | grep processor | grep -v 0 | wc -l)
job_pool_init $poolSize 1
for f in $(find ./ -regextype posix-extended -regex '.*\.(css|go|js|json|php|pl|rb|toml|sh|xml|yml)$' $@) ; do
  fullfile=$(basename "$f")
  fext="${fullfile##*.}"
  fname="${fulfile%.*}"
  case $fext in
    css)
      argv="css"
    ;;
    go)
      argv="go"
    ;;
    js)
      argv="javascript"
    ;;
    json)
      argv="json"
    ;;
    php)
      argv="php"
    ;;
    pl)
      argv="perl"
    ;;
    rb)
      argv="ruby"
    ;;
    sh)
      argv="bash"
    ;;
    toml)
      argv="toml"
    ;;
    xml)
      argv="xml"
    ;;
    yml)
      argv="yaml"
    ;;
    *)
      echo "$ques_panel unknown or unsupported language ($f)"
      continue
    ;;
  esac 
  job_pool_run lang_wraper $argv $f
done
job_pool_wait
job_pool_shutdown
set_return ${job_pool_nerrors}
exit $RETURN
