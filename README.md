```
███████╗██╗   ██╗███╗   ██╗████████╗ █████╗ ██╗  ██╗    ███████╗██████╗ ██████╗  ██████╗ ██████╗
██╔════╝╚██╗ ██╔╝████╗  ██║╚══██╔══╝██╔══██╗╚██╗██╔╝    ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗
███████╗ ╚████╔╝ ██╔██╗ ██║   ██║   ███████║ ╚███╔╝     █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝
╚════██║  ╚██╔╝  ██║╚██╗██║   ██║   ██╔══██║ ██╔██╗     ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗
███████║   ██║   ██║ ╚████║   ██║   ██║  ██║██╔╝ ██╗    ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║
╚══════╝   ╚═╝   ╚═╝  ╚═══╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝
```


# syntaxerror, a multi-language parser and validator commandline utility (LINT) 

with no option, `syntaxerror` will recursively parse all files in the current directory to analyse them and search for syntax errors. `syntaxerror` will exit and return 1 at the first error found in a file. If you use option `-f` or `--force`, `syntaxerror` will continue parsing until the last file.

if one error is found, `syntaxerror` will exit with status code 1 (error)

`syntaxerror` support theses languages :
  - bash-script
  - css
  - golang
  - javascript
  - json
  - perl
  - php
  - python
  - toml
  - ruby
  - xml
  - yaml

You can pass any `find` argument after `-f`|`--force` if you need to exclude file or path

This example will exlude directory `./badjs/`:
```bash
syntaxerror ! -path "./badjs"
```

This example will exlude all css files:
```bash
syntaxerror -f ! -name "*.css"
```


## Prerequisites
syntaxerror is compatible with any debian-like system

## Installing
* To install the latest stable version, execute as root or with sudo : 
```
wget -qO- https://gitlab.com/pubcro/syntaxerror/raw/master/install | bash
```


## Upgrade :
* To upgrading to the latest stable version, execute as root or with sudo : 
```
wget -qO- https://gitlab.com/pubcro/syntaxerror/raw/master/install | bash
```

## Usage
main command is `syntaxerror`

Usage : 
```
syntaxerror [option]
```

### Available options :
* -f|--force
force syntaxerrror to continue to parse other files after detecting an error
